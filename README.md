# README #

Project ini merupakan mini project yang mengsimulasikan service-service yang digunakan pada perbankan, meliputi:

1. Pengelolaan Data Kredit

2. Pengelolaan Data Nasabah

3. Pengelolaan Data Pegawai

# Kredit Service #

Link Repository: https://bitbucket.org/christiantojimmy/kredit-service

Fitur:

1. Pengelolaan data kredit

2. Pengelolaan data tipe kredit

3. Pengelolaan data pembayaran kredit

# Nasabah Service #

Link Repository: https://bitbucket.org/christiantojimmy/nasabah-service

Fitur:

1. Pengelolaan data nasabah

2. Pengelolaan data domisili

3. Pengelolaan data cabang

# Pegawai Service #

Link Repository: https://bitbucket.org/christiantojimmy/pegawai-service

Fitur:

1. Pengelolaan data pegawai

2. Pengelolaan data departemen

3. Pengelolaan data jabatan

